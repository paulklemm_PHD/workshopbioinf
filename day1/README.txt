
###############################
# String manipulation -- sed
###############################

date +"%d.%m.%y" >testfile.txt

echo "Paul" >>testfile.txt

sed 's/[^a-zA-Z0-9]//g' testfile.txt | sed 's/[aeiouAEIOU]/X/g' | sed 's/[0-9]/Y/g' >>testfile2.txt

###############################
# String manipulation -- rev tr
###############################

echo "TCGTATATAGCT" > some_dna_sequence.txt 
cat some_dna_sequence.txt | tr 'ACGT' 'TGCA' | rev > cdna_sequence.txt
cat some_dna_sequence.txt | tr 'ACGT' 'UGCA' | rev >rna_sequence.txt

###############################
# Fetching data + working with files 
###############################

wget www.toxido.de/titanic3.xls
md5sum titanic3.xls >titanic3.xls.md5sum
cat titanic3.csv

LC_ALL=C sort -t$'\t' -k5,5nr titanic3.csv

# Who is the oldest female? 
LC_ALL=C sort -t$'\t' -k4,4 -k5,5nr titanic3.csv
# Mrs. Tyrell William (Julia Florence Siegel) is the oldest woman

# What are the 5 most expensive tickets? 
LC_ALL=C sort -t$'\t' -k9,9nr titanic3.csv | head 
# 512.3292, 512.3292, 512.3292, 512.3292, 263.0000

LC_ALL=C sort -t$'\t' -k9,9nr titanic3.csv | head -n4

(...)